## Super Hash

```bash
composer require flexice/supers-hash
```

```php
    //Метод для хеширования
    superhash::hash("Строка","Тип хеширования по стандарту default-sha256");
```

Список типов

 - default-sha256 **[64 SYMBOLS]**
 - default-md5 **[32 SYMBOLS]**
 - default-sha512 **[128 SYMBOLS]**
 - default-gost **[64 SYMBOLS]**
 - gost-plus **[64 SYMBOLS]**
 - whirlpool **[128 SYMBOLS]**
 - struct4 **[64 SYMBOLS]**
 
```php
echo superhash::hash("строка","gost-plus");
```