<?php


namespace flexice\php;


class superhash
{
    public function hash($string,$type='default-sha256')
    {
        switch ($type)
        {
            case 'default-md5':
                return hash("md5", $string);
                break;
            case 'default-sha512':
                return hash("sha512", $string);
                break;
            case 'default-gost':
                return hash("gost", $string);
                break;
            case 'gost-plus':
                $rstring='';
                $string = str_split($string);
                foreach ($string as $item)
                {
                    $rstring = $rstring.hash("gost",$item);
                }
                return hash("gost",$rstring);
                break;
            case 'whirlpool':
                return hash("whirlpool",$string);
                break;
            case 'struct4':
                $nextString = base64_encode($string);
                $nextString = base64_encode(strval($nextString));
                $nextString = strval($nextString);
                $nextString = hash("haval256,5",$nextString);
                $nextString = strval($nextString);
                $lg = strlen($nextString);
                if ($lg <= 64)
                {
                    $nextString = $nextString.$lg;
                }
                return $nextString;
                break;
            default:
                return hash("sha256", $string);
                break;
        }
    }
}